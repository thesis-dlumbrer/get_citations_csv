#! /usr/bin/env python

import datetime
import os
import csv

import scholarly

CSV_FILE_PATH = "papers.csv"
TITLE_MAIN_PAPER = 'Visualizing software systems as cities'

date = datetime.datetime.now().isoformat()
print("Starting...", date)

papers = dict()
# Retrieve the paper's data, fill-in, and print
search_query = scholarly.scholarly.search_pubs(TITLE_MAIN_PAPER)
print("Query done")

paper = next(search_query).fill()

#print('Name:', author.name)
#print('Cites, cites 5y, h-index:',
#      author.citedby, author.citedby5y, author.hindex)
    
papers[date] = paper

# Export CSV citations
if os.path.exists(CSV_FILE_PATH):
    os.remove(CSV_FILE_PATH)
with open(CSV_FILE_PATH, 'a') as f:
    writer = csv.writer(f)
    for citation in paper.citedby:
        writer.writerow(citation.bib.values())
    
print("Exit OK")

# papers.close()

# author = next(search_query)
# print(author.citedby)

# Print the titles of the author's publications
# print([pub.bib['title'] for pub in author.publications])
# print([pub.bib for pub in author.publications])

# Take a closer look at the first publication
# pub = author.publications[0].fill()
# print(pub)

# Which papers cited that publication?
# print([citation.bib['title'] for citation in paper.citedby])