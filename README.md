# Get Citations Csv

Using Scholarly

To execute:

1. Install `requeriments.txt`
2. Change global variables CSV_FILE_PATH and TITLE_MAIN_PAPER in `get_papers.py`
3. Execute `get_papers.py`

### Install geckodriver on Ubuntu

- wget https://github.com/mozilla/geckodriver/releases/download/v0.24.0/geckodriver-v0.24.0-linux64.tar.gz
- tar -xvzf geckodriver*
- chmod +x geckodriver
- sudo mv geckodriver /usr/local/bin/ 